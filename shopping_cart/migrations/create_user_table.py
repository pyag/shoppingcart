from db_operations import init
import mysql.connector as db

def run():
  try:
    connection = init.init()
    cursor = connection.cursor()

    create_user = ("CREATE TABLE user("
      "username VARCHAR(100) PRIMARY KEY, "
      "password VARCHAR(100) NOT NULL, "
      "role VARCHAR(100) NOT NULL"
      ");")

    cursor.execute(create_user)
    connection.commit()
    print('User table created ....OK')

    cursor.close()
    connection.close()
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err
