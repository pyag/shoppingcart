from db_operations import init
import mysql.connector as db

def run():
  try:
    connection = init.init()
    cursor = connection.cursor()

    create_product = ("CREATE TABLE product("
      "product_id INT AUTO_INCREMENT PRIMARY KEY, "
      "name VARCHAR(100) NOT NULL, "
      "price INT NOT NULL"
      ");")

    cursor.execute(create_product)
    connection.commit()
    print('Product table created ....OK')

    cursor.close()
    connection.close()
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err
