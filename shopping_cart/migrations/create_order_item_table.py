from db_operations import init
import mysql.connector as db

def run():
  try:
    connection = init.init()
    cursor = connection.cursor()

    create_order_item = ("CREATE TABLE order_item("
      "order_id INT NOT NULL, "
      "product_id INT NOT NULL"
      ");")

    cursor.execute(create_order_item)
    connection.commit()
    print('Order Item table created ....OK')

    cursor.close()
    connection.close()
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err
