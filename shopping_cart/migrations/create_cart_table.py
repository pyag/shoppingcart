from db_operations import init
import mysql.connector as db

def run():
  try:
    connection = init.init()
    cursor = connection.cursor()

    create_cart = ("CREATE TABLE cart("
      "product_id INT NOT NULL, "
      "username VARCHAR(100) NOT NULL"
      ");")

    cursor.execute(create_cart)
    connection.commit()
    print('Cart table created ....OK')

    cursor.close()
    connection.close()
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err
