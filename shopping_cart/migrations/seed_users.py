from db_operations import init
import mysql.connector as db

def run():
  try:
    connection = init.init()
    cursor = connection.cursor()

    insert_admin = ("INSERT INTO user VALUES('admin', 'password', 'admin')");
    insert_user = ("INSERT INTO user VALUES('user', 'password', 'customer')")

    cursor.execute(insert_admin)
    cursor.execute(insert_user)
    connection.commit()
    print('Admin and user inserted ....OK')

    cursor.close()
    connection.close()
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err
