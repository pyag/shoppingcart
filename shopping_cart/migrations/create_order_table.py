from db_operations import init
import mysql.connector as db

def run():
  try:
    connection = init.init()
    cursor = connection.cursor()

    create_order = ("CREATE TABLE order_table("
      "id INT AUTO_INCREMENT PRIMARY KEY, "
      "username VARCHAR(100) NOT NULL, "
      "total_price INT NOT NULL"
      ");")

    cursor.execute(create_order)
    connection.commit()
    print('Order table created ....OK')

    cursor.close()
    connection.close()
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err
