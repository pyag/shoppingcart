from db_operations import db_cart, db_product

class CartController:
  '''
  Cart Controller handles everything related
  to Cart Operations.
  '''

  def show_cart(self, username):
    cart_items = db_cart.show_cart(username)

    if (cart_items):
      print('\nCart items\n')
      for record in cart_items:
        print('Product ID: {}'.format(record[0]))
        print('Name: {}'.format(record[1]))
        print('Price: {}\n'.format(record[2]))        
    else:
      print('Cart is Empty')


  def add_to_cart(self, username):
    try:
      input_product_ids = input('\nEnter the comma seperated product ids: ')
      input_product_ids = [int(x) for x in input_product_ids.split(',')]

      ## getting the already present products
      result = db_product.show_products()
      product_id_list = []
      
      ## preparing a list of products ids that
      ## are already present
      for record in result:
        product_id_list.append(record[0])


      ## If the input id is present in actual
      ## product id list, add to cart, otherwise
      ## ignore it.
      for product_id in input_product_ids:
        if product_id in product_id_list:
          db_cart.add_to_cart(product_id, username)

    except ValueError as err:
      print('ERROR: {}'.format(err))

  def remove_from_cart(self):
    try:
      delete_product_id = int(input('\nEnter product id to delete from cart: '))

      ## retrieving the actual product ids
      ## from product database and listing
      ## them
      result = db_product.show_products()
      product_id_list = []
      for record in result:
        product_id_list.append(record[0])

      ## checking if delete input id is 
      ## present in actual product id list
      if delete_product_id in product_id_list:
        db_cart.remove_from_cart(delete_product_id)

    except ValueError as err:
      print('ERROR: {}'.format(err))