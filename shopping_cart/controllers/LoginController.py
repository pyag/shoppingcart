from db_operations import db_login

from controllers.ProductController import ProductController
from controllers.CartController import CartController
from controllers.OrderController import OrderController

class LoginController:
  '''
  Login Controller to process the flow
  of login and next processes. The code
  in this controller is self explainatory.
  '''

  def login(self):
    print('\nLogin to continue')
    username = input('Enter username: ')
    password = input('Enter password: ')

    print('Loading ....\n')

    result = db_login.authenticate(username, password)
    if (result):

      if (result[2] == 'admin'):
        print('Successfully logged in as admin\n')

        while True:
          status = self.admin(username)
          if (status == 0):
            break

      else:
        print('Successfully logged in as customer\n')
      
        while True:
          status = self.user(username)
          if (status == 0):
            break

    else:
      print('Invalid credentials...')
      print('Exiting...')
      exit()


  def user(self, username):

    print('Select one of the following options...')
    print('1. View Products')
    print('2. View Cart')
    print('3. View Order history')
    print('4. Add to Cart')
    print('5. Delete from Cart')
    print('6. Place an Order')
    print('7. Logout')

    selection = None

    while True:
      try:
        selection = int(input('Enter your option: '))
        break
      except ValueError as err:
        print('ERROR: Please enter a options in numeric value'.format(err))

    if (selection == 1):
      product_controller = ProductController()
      product_controller.show_products(username)
    elif (selection == 2):
      cart_controller = CartController()
      cart_controller.show_cart(username)
    elif (selection == 3):
      order_controller = OrderController()
      order_controller.show_orders(username)
    elif (selection == 4):
      cart_controller = CartController()
      cart_controller.add_to_cart(username)
    elif (selection == 5):
      cart_controller = CartController()
      cart_controller.remove_from_cart()
    elif (selection == 6):
      order_controller = OrderController()
      order_controller.place_order(username)
    elif (selection == 7):
      return 0;
    else:
      print('Bad Selection')

    return 1

  def admin(self, username):
    
    print('Select one of the following options...')
    print('1. View Products')
    print('2. Add a product')
    print('3. Delete a product')
    print('4. View Orders')
    print('5. Logout')

    selection = None
    
    while True:
      try:
        selection = int(input('Enter your option: '))
        break
      except ValueError as err:
        print('ERROR: Please enter a options in numeric value'.format(err))

    if (selection == 1):
      product_controller = ProductController()
      product_controller.show_products(username)
    elif (selection == 2):
      product_controller = ProductController()
      product_controller.add_product()
    elif (selection == 3):
      product_controller = ProductController()
      product_controller.remove_product()
    elif (selection == 4):
      order_controller = OrderController()
      order_controller.show_orders('user')
    elif (selection == 5):
      return 0;
    else:
      print('Bad Selection')

    return 1;
