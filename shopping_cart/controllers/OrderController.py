from db_operations import db_order, db_cart
from pandas import DataFrame

class OrderController:
  '''
  This controller is responsible for handling
  order related processing.
  '''
  
  def show_orders(self, username):
    order_list = db_order.show_orders(username)

    if (order_list):
      for record in order_list:
        self.view_order_by_id(int(record[0]))
    else:
      print('No Orders yet')

  
  def place_order(self, username):
    cart_items = db_cart.show_cart(username)
    total_price = 0

    ## calculating the total price of order
    for record in cart_items:
      total_price += int(record[2])

    db_order.place_order(username, total_price)
    orders = db_order.show_orders(username)

    ## getting the order id by retrieving
    ## last order insert in the database
    ## above
    last_order = None
    if (orders):
      last_order = orders[len(orders)-1]

    order_id = 1
    if (last_order):
      order_id = last_order[0]

    print('order id is = {}'.format(order_id))

    ## inserting the product in order items
    ## and simultaneously removing the same
    ## item from cart.
    for record in cart_items:
      db_order.push_order_items(order_id, record[0])
      db_cart.remove_from_cart(record[0])

    print('\nYour order is placed!\n')  

  def view_order_by_id(self, order_id):
    order_items = db_order.get_order_items(order_id)

    view_report = {
      'id': [],
      'name': [],
      'price': []
    }

    total_price = 0
    if (order_items):
      print('Order list with order id: {}'.format(order_id))
      for record in order_items:
        view_report['id'].append(record[0])
        view_report['name'].append(record[1])
        view_report['price'].append(record[2])
        total_price += int(record[2])

      df = DataFrame(view_report)
      print(df)
      print('Total price = {}\n'.format(total_price))
    else:
      print('No order to show')