from db_operations import db_product

class ProductController:
  '''
  This controller handles the operations related
  to product adding and retieving. The code in
  this controller is self explainatory
  '''

  def show_products(self, username):

    print('\nProduct List')

    product_list = db_product.show_products()
    if (product_list):
      print('Count: {}\n'.format(len(product_list)))

      for record in product_list:
        print('ID: {}'.format(record[0]))
        print('Name: {}'.format(record[1]))
        print('Price: {}\n'.format(record[2]))
    else:
      print('No product to show')


  def add_product(self):

    name = input('\nEnter product name: ')
    price = None

    while True:
      try:
        price = int(input('Enter product price: '))
        break
      except ValueError as err:
        print('ERROR: Please enter a options in numeric value'.format(err))
      
    db_product.add_product(name, price)

    return True

  def remove_product(self):
    product_id = None
    try:
      product_id = int(input('\nEnter product id to delete: '))
    except ValueError as err:
      print('ERROR: Please enter a options in numeric value'.format(err))
      raise err

    db_product.remove_product(product_id)

    print('\nProduct with id: {} deleted\n'.format(product_id))