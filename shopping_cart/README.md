# Welcome to shopping Cart

## Prerequisite
* Python v3.5 or greater
* MySQL

## How to setup the project and run it
* Make virual environment  
  On linux or macOS systems run:  
  `$ python3 -m venv <env-name>`  
  `$ source <env-name>/bin/activate`  
  On windows run:  
  `$ python -m venv <env-name>`  
  `$ <env-name>\Scripts\activate.bat`  
  For more information, refer https://docs.python.org/3/tutorial/venv.html
    

## Install dependencies
* Run command  
  `$ cd shoppingcart/shopping_cart`  
  `$ pip install -r requirements.txt`  

## Create Database and configure it in project
* Open MySQL console  
  ```
  $ mysql -u <user> -p
  ```  
  `Press enter and type the user's password`  

* Create database for shopping cart in MySQL console.  
  `mysql> CREATE DATABASE shoppingCart;`  
  `mysql> exit;`  

* Configure the db settings in project  
  Change the config according to your database.  
  * Open file located at `./config/config.py`  
  * Fill in information according to your database settings.  

## Run migrations
* Run file `migration.py`. Command is below:  
  `$ python migration.py`  

## You are all set now! Run the Shopping Cart App
* Run file `shopping_cart.py`. Command is below:  
  `$ python shopping_cart.py`  

* To login as a customer, credentials are:  
  `username: user`  
  `password: password`  

* To login as a admin, credentials are:  
  `username: admin`  
  `password: password`  

