'''
This file runs all the migrations to create and seed
the database tables. To add a migration file, add a 
file in migrations folder and do all the stuff in run
function. Import your migration file here and call the
run function here.
'''

from migrations import create_user_table
from migrations import create_product_table
from migrations import create_cart_table
from migrations import create_order_table
from migrations import create_order_item_table
from migrations import seed_users

def run():
  try:
    create_user_table.run()
    create_product_table.run()
    create_cart_table.run()
    create_order_table.run()
    create_order_item_table.run()
    seed_users.run()
  except:
    print('Something went wrong')

run()