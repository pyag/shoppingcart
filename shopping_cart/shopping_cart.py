from controllers.LoginController import LoginController

def main():
  print('\nWelcome to Shopping Cart ...\n')
	
  while True:
    option = input("Press ENTER to continue OR type 'exit' to exit shopping cart: ")
    	
    if (option == 'exit'):
      break

    login_controller = LoginController()
    login_controller.login()

  print('Bye. See you soon')

if __name__ == '__main__':
  main()