from db_operations import init
import mysql.connector as db

def show_orders(*username):
  try:
    selectOrders = ("SELECT * FROM order_table WHERE username = (%s)")

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(selectOrders, username)

    result = cursor.fetchall()

    return result
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err


def place_order(*order_details):
  try:
    insert_order = ("INSERT INTO order_table(username, total_price)"
      " VALUES (%s, %s)")

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(insert_order, order_details)
    connection.commit()

    return True
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err


def push_order_items(*order_items):
  try:
    insert_order_items = ("INSERT INTO order_item(order_id, product_id)"
      " VALUES (%s, %s)")

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(insert_order_items, order_items)
    connection.commit()

    return True
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err


def get_order_items(*order_id):
  try:
    select_order_items = ("SELECT p.product_id, p.name, p.price "
      "FROM order_item o, product p WHERE o.product_id = p.product_id "
      "AND o.order_id = (%s)")

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(select_order_items, order_id)
    result = cursor.fetchall()

    return result
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err