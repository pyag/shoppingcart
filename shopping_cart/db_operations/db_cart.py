from db_operations import init
import mysql.connector as db

def show_cart(*username):
  try:
    select_cart = ("SELECT c.product_id, p.name, p.price "
      "FROM product p, cart c "
      "WHERE c.product_id = p.product_id AND c.username = (%s)")

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(select_cart, username)

    result = cursor.fetchall()

    connection.close()
    cursor.close()

    return result
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err


def add_to_cart(*cart_info):
  try:
    insert_cart = ("INSERT INTO cart(product_id, username) VALUES (%s, %s)")

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(insert_cart, cart_info)
    connection.commit()

    connection.close()
    cursor.close()

    return True
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err


def remove_from_cart(*cart_info):
  try:
    delete_cart = ("DELETE FROM cart WHERE product_id = (%s)")

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(delete_cart, cart_info)
    connection.commit()

    connection.close()
    cursor.close()

    return True
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err