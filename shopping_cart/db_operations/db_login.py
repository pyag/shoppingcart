from db_operations import init
import mysql.connector as db

def authenticate(*user_info):
  try:
    selectUser = "SELECT * FROM user WHERE username = (%s) AND PASSWORD = (%s)"

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(selectUser, user_info)
    result = cursor.fetchone()

    return result
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err  