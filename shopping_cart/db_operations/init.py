import mysql.connector as db
from config import config

def init():
  try:
    dbConfigs = config.databaseConfigs["development"]
    connection = db.connect(**dbConfigs)
    return connection
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err