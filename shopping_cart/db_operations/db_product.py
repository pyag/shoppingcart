from db_operations import init
import mysql.connector as db

def show_products():
  try:
    select_product = "SELECT * FROM product"

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(select_product)
    result = cursor.fetchall()

    return result
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err

def add_product(*product_details):
  try:
    insert_product = "INSERT INTO product(name, price) VALUES (%s, %s)"

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(insert_product, product_details)
    connection.commit()

    return True
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err

def remove_product(*product_id):
  try:
    delete_product = "DELETE FROM product WHERE product_id = (%s)"

    connection = init.init()
    cursor = connection.cursor()

    cursor.execute(delete_product, product_id)
    connection.commit()

    return True
    
  except db.Error as err:
    print("Something went wrong: {}".format(err))
    raise err